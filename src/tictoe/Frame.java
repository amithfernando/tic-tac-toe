/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Frame.java
 *
 * Created on May 28, 2009, 6:35:56 AM
 */
package tictoe;

import java.io.InputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Amith Ishanka
 */
public class Frame extends javax.swing.JFrame {

    Map A = new TreeMap();
    Map B = new TreeMap();
    Map C = new TreeMap();
    int Player1Times = 0;
    int Player2Times = 0;
    int clickTimes = 0;
    int port = 11001;
    int portServer = 11000;//server port here

    public Frame() {
        initComponents();
        A.put(0, "");
        A.put(1, "");
        A.put(2, "");
        B.put(0, "");
        B.put(1, "");
        B.put(2, "");
        C.put(0, "");
        C.put(1, "");
        C.put(2, "");
    }

    private Frame(int i) {
    }

    public boolean checkClickTimes() {
        boolean b = true;
        if (clickTimes == 9) {
            b = false;
        }
        System.out.println("cl times " + clickTimes);
        return b;
    }

    public void checkWinings() {
        try {
            //x wins
            if (A.get(0).toString().equals("X") & A.get(1).toString().equals("X") & A.get(2).toString().equals("X")) {
                JOptionPane.showMessageDialog(this, "x WINS");
            } else if (A.get(0).toString().equals("X") & B.get(0).toString().equals("X") & C.get(0).toString().equals("X")) {
                JOptionPane.showMessageDialog(this, "x WINS");
            } else if (A.get(0).toString().equals("X") & B.get(1).toString().equals("X") & C.get(2).toString().equals("X")) {
                JOptionPane.showMessageDialog(this, "x WINS");
            } else if (A.get(1).toString().equals("X") & B.get(1).toString().equals("X") & C.get(1).toString().equals("X")) {
                JOptionPane.showMessageDialog(this, "x WINS");
            } else if (A.get(2).toString().equals("X") & B.get(2).toString().equals("X") & C.get(2).toString().equals("X")) {
                JOptionPane.showMessageDialog(this, "x WINS");
            } else if (A.get(2).toString().equals("X") & B.get(1).toString().equals("X") & C.get(0).toString().equals("X")) {
                JOptionPane.showMessageDialog(this, "x WINS");
            } else if (B.get(0).toString().equals("X") & B.get(1).toString().equals("X") & B.get(2).toString().equals("X")) {
                JOptionPane.showMessageDialog(this, "x WINS");
            } else if (C.get(0).toString().equals("X") & C.get(1).toString().equals("X") & C.get(2).toString().equals("X")) {
                JOptionPane.showMessageDialog(this, "x WINS");
            }
            //o wins
            if (A.get(0).toString().equals("O") & A.get(1).toString().equals("O") & A.get(2).toString().equals("O")) {
                JOptionPane.showMessageDialog(this, "O WINS");
            } else if (A.get(0).toString().equals("O") & B.get(0).toString().equals("O") & C.get(0).toString().equals("O")) {
                JOptionPane.showMessageDialog(this, "O WINS");
            } else if (A.get(0).toString().equals("O") & B.get(1).toString().equals("O") & C.get(2).toString().equals("O")) {
                JOptionPane.showMessageDialog(this, "x WINS");
            } else if (A.get(1).toString().equals("O") & B.get(1).toString().equals("O") & C.get(1).toString().equals("O")) {
                JOptionPane.showMessageDialog(this, "O WINS");
            } else if (A.get(2).toString().equals("O") & B.get(2).toString().equals("O") & C.get(2).toString().equals("O")) {
                JOptionPane.showMessageDialog(this, "O WINS");
            } else if (A.get(2).toString().equals("O") & B.get(1).toString().equals("O") & C.get(0).toString().equals("O")) {
                JOptionPane.showMessageDialog(this, "O WINS");
            } else if (B.get(0).toString().equals("O") & B.get(1).toString().equals("O") & B.get(2).toString().equals("O")) {
                JOptionPane.showMessageDialog(this, "x WINS");
            } else if (C.get(0).toString().equals("O") & C.get(1).toString().equals("O") & C.get(2).toString().equals("O")) {
                JOptionPane.showMessageDialog(this, "O WINS");
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        b1 = new javax.swing.JLabel();
        b2 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        a1 = new javax.swing.JLabel();
        b0 = new javax.swing.JLabel();
        c1 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        a2 = new javax.swing.JLabel();
        c2 = new javax.swing.JLabel();
        c0 = new javax.swing.JLabel();
        a0 = new javax.swing.JLabel();
        btnStartNew = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtIpAdd = new javax.swing.JTextField();
        txtServerPort = new javax.swing.JTextField();
        txtClport = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        btnStartServer = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Tic Toe ( talkamith.blogspot.com )");

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        b1.setBackground(new java.awt.Color(255, 255, 0));
        b1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        b1.setForeground(new java.awt.Color(0, 0, 102));
        b1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        b1.setText("###");
        b1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                b1MouseClicked(evt);
            }
        });
        jPanel1.add(b1, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 150, -1, -1));

        b2.setBackground(new java.awt.Color(255, 255, 0));
        b2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        b2.setForeground(new java.awt.Color(0, 0, 102));
        b2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        b2.setText("###");
        b2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                b2MouseClicked(evt);
            }
        });
        jPanel1.add(b2, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 150, -1, -1));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 160, 480, -1));

        a1.setBackground(new java.awt.Color(255, 255, 0));
        a1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        a1.setForeground(new java.awt.Color(0, 0, 102));
        a1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        a1.setText("###");
        a1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                a1MouseClicked(evt);
            }
        });
        jPanel1.add(a1, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 0, -1, -1));

        b0.setBackground(new java.awt.Color(255, 255, 0));
        b0.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        b0.setForeground(new java.awt.Color(0, 0, 102));
        b0.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        b0.setText("###");
        b0.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                b0MouseClicked(evt);
            }
        });
        jPanel1.add(b0, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 150, -1, -1));

        c1.setBackground(new java.awt.Color(255, 255, 0));
        c1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        c1.setForeground(new java.awt.Color(0, 0, 102));
        c1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        c1.setText("###");
        c1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                c1MouseClicked(evt);
            }
        });
        jPanel1.add(c1, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 320, -1, -1));

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 0, -1, 340));

        a2.setBackground(new java.awt.Color(255, 255, 0));
        a2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        a2.setForeground(new java.awt.Color(0, 0, 102));
        a2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        a2.setText("###");
        a2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                a2MouseClicked(evt);
            }
        });
        jPanel1.add(a2, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 0, -1, -1));

        c2.setBackground(new java.awt.Color(255, 255, 0));
        c2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        c2.setForeground(new java.awt.Color(0, 0, 102));
        c2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        c2.setText("###");
        c2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                c2MouseClicked(evt);
            }
        });
        jPanel1.add(c2, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 320, -1, -1));

        c0.setBackground(new java.awt.Color(255, 255, 0));
        c0.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        c0.setForeground(new java.awt.Color(0, 0, 102));
        c0.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        c0.setText("###");
        c0.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                c0MouseClicked(evt);
            }
        });
        jPanel1.add(c0, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 320, -1, -1));

        a0.setBackground(new java.awt.Color(255, 255, 0));
        a0.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        a0.setForeground(new java.awt.Color(0, 0, 102));
        a0.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        a0.setText("###");
        a0.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                a0MouseClicked(evt);
            }
        });
        jPanel1.add(a0, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        btnStartNew.setText("Start New (clear all)");
        btnStartNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStartNewActionPerformed(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Multi Player"));

        jLabel1.setText("IP Address (other)");

        jLabel2.setText("Listening Port");

        jLabel3.setText("Receving Port");

        jLabel4.setText("Tic Toe ver 1.0    amithfernando.com ");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addGap(32, 32, 32)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtIpAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(txtServerPort, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtClport, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE))))
                    .addComponent(jLabel4))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtIpAdd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtServerPort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtClport, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addComponent(jLabel4)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnStartServer.setText("Start Server");
        btnStartServer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStartServerActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(btnStartNew)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnStartServer))
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(32, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnStartNew)
                    .addComponent(btnStartServer))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        setSize(new java.awt.Dimension(535, 577));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void a0MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_a0MouseClicked

        if (checkClickTimes()) {
            if (a0.getText().equals("X") || a0.getText().equals("O")) {
                JOptionPane.showMessageDialog(this, "Alredy set!");
            } else {
                if (clickTimes == 0 || clickTimes == 2 || clickTimes == 4 || clickTimes == 6 || clickTimes == 8) {
                    a0.setText("X");
                    clickTimes++;
                    Player1Times++;
                    A.put(0, "X");
                    sendPoint("A0!X");
                } else {
                    a0.setText("O");
                    clickTimes++;
                    Player2Times++;
                    A.put(0, "O");
                    sendPoint("A0!O");
                }
            }
        } else {
            checkWinings();
        }
        checkWinings();
    }//GEN-LAST:event_a0MouseClicked

    private void a1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_a1MouseClicked

        if (checkClickTimes()) {
            if (a1.getText().equals("X") || a1.getText().equals("O")) {
                JOptionPane.showMessageDialog(this, "Alredy set!");
            } else {
                if (clickTimes == 0 || clickTimes == 2 || clickTimes == 4 || clickTimes == 6 || clickTimes == 8) {
                    a1.setText("X");
                    clickTimes++;
                    Player1Times++;
                    A.put(1, "X");
                    sendPoint("A1!X");
                } else {
                    a1.setText("O");
                    clickTimes++;
                    Player2Times++;
                    A.put(1, "O");
                    sendPoint("A1!O");
                }
            }
        } else {
            checkWinings();
        }
        checkWinings();
    }//GEN-LAST:event_a1MouseClicked

    private void a2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_a2MouseClicked

        if (checkClickTimes()) {
            if (a2.getText().equals("X") || a2.getText().equals("O")) {
                JOptionPane.showMessageDialog(this, "Alredy set!");
            } else {
                if (clickTimes == 0 || clickTimes == 2 || clickTimes == 4 || clickTimes == 6 || clickTimes == 8) {
                    a2.setText("X");
                    clickTimes++;
                    Player1Times++;
                    A.put(2, "X");
                    sendPoint("A2!X");
                } else {
                    a2.setText("O");
                    clickTimes++;
                    Player2Times++;
                    A.put(2, "O");
                    sendPoint("A2!O");
                }
            }
        } else {
            checkWinings();
        }
        checkWinings();
    }//GEN-LAST:event_a2MouseClicked

    private void b0MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_b0MouseClicked

        if (checkClickTimes()) {
            if (b0.getText().equals("X") || b0.getText().equals("O")) {
                JOptionPane.showMessageDialog(this, "Alredy set!");
            } else {
                if (clickTimes == 0 || clickTimes == 2 || clickTimes == 4 || clickTimes == 6 || clickTimes == 8) {
                    b0.setText("X");
                    clickTimes++;
                    Player1Times++;
                    B.put(0, "X");
                    sendPoint("B0!X");
                } else {
                    b0.setText("O");
                    clickTimes++;
                    Player2Times++;
                    B.put(0, "O");
                    sendPoint("B0!O");
                }
            }
        } else {
            checkWinings();
        }
        checkWinings();
    }//GEN-LAST:event_b0MouseClicked

    private void b1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_b1MouseClicked

        if (checkClickTimes()) {
            if (b1.getText().equals("X") || b1.getText().equals("O")) {
                JOptionPane.showMessageDialog(this, "Alredy set!");
            } else {
                if (clickTimes == 0 || clickTimes == 2 || clickTimes == 4 || clickTimes == 6 || clickTimes == 84) {
                    b1.setText("X");
                    clickTimes++;
                    Player1Times++;
                    B.put(1, "X");
                    sendPoint("B1!X");
                } else {
                    b1.setText("O");
                    clickTimes++;
                    Player2Times++;
                    B.put(1, "O");
                    sendPoint("B1!O");
                }
            }
        } else {
            checkWinings();
        }
        checkWinings();
    }//GEN-LAST:event_b1MouseClicked

    private void b2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_b2MouseClicked

        if (checkClickTimes()) {
            if (b2.getText().equals("X") || b2.getText().equals("O")) {
                JOptionPane.showMessageDialog(this, "Alredy set!");
            } else {
                if (clickTimes == 0 || clickTimes == 2 || clickTimes == 4 || clickTimes == 6 || clickTimes == 8) {
                    b2.setText("X");
                    clickTimes++;
                    Player1Times++;
                    B.put(2, "X");
                    sendPoint("B2!X");
                } else {
                    b2.setText("O");
                    clickTimes++;
                    Player2Times++;
                    B.put(2, "O");
                    sendPoint("B2!O");
                }
            }
        } else {
            checkWinings();
        }
        checkWinings();
    }//GEN-LAST:event_b2MouseClicked

    private void c0MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_c0MouseClicked

        if (checkClickTimes()) {
            if (c0.getText().equals("X") || c0.getText().equals("O")) {
                JOptionPane.showMessageDialog(this, "Alredy set!");
            } else {
                if (clickTimes == 0 || clickTimes == 2 || clickTimes == 4 || clickTimes == 6 || clickTimes == 8) {
                    c0.setText("X");
                    clickTimes++;
                    Player1Times++;
                    C.put(0, "X");
                    sendPoint("C0!X");
                } else {
                    c0.setText("O");
                    clickTimes++;
                    Player2Times++;
                    C.put(0, "O");
                    sendPoint("C0!O");
                }
            }
        } else {
            checkWinings();
        }
        checkWinings();
    }//GEN-LAST:event_c0MouseClicked

    private void c1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_c1MouseClicked

        if (checkClickTimes()) {
            if (c1.getText().equals("X") || c1.getText().equals("O")) {
                JOptionPane.showMessageDialog(this, "Alredy set!");
            } else {
                if (clickTimes == 0 || clickTimes == 2 || clickTimes == 4 || clickTimes == 6 || clickTimes == 8) {
                    c1.setText("X");
                    clickTimes++;
                    Player1Times++;
                    C.put(1, "X");
                    sendPoint("C1!X");
                } else {
                    c1.setText("O");
                    clickTimes++;
                    Player2Times++;
                    C.put(1, "O");
                    sendPoint("C1!O");
                }
            }
        } else {
            checkWinings();
        }
        checkWinings();
    }//GEN-LAST:event_c1MouseClicked

    private void c2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_c2MouseClicked

        if (checkClickTimes()) {
            if (c2.getText().equals("X") || c2.getText().equals("O")) {
                JOptionPane.showMessageDialog(this, "Alredy set!");
            } else {
                if (clickTimes == 0 || clickTimes == 2 || clickTimes == 4 || clickTimes == 6 || clickTimes == 8) {
                    c2.setText("X");
                    clickTimes++;
                    Player1Times++;
                    C.put(2, "X");
                    sendPoint("C2!X");
                } else {
                    c2.setText("O");
                    clickTimes++;
                    Player2Times++;
                    C.put(2, "O");
                    sendPoint("C2!O");
                }
            }
        } else {
            checkWinings();
        }
        checkWinings();
    }//GEN-LAST:event_c2MouseClicked

    private void btnStartNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStartNewActionPerformed
        try {
            startNewGame();
            MainClient.mySendCommand("#NewGame#", txtIpAdd.getText(), port);
        } catch (Exception ex) {
            Logger.getLogger(Frame.class.getName()).log(Level.SEVERE, null, ex);
        }
}//GEN-LAST:event_btnStartNewActionPerformed

    private void btnStartServerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStartServerActionPerformed
        myStartServer();
}//GEN-LAST:event_btnStartServerActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new Frame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel a0;
    private javax.swing.JLabel a1;
    private javax.swing.JLabel a2;
    private javax.swing.JLabel b0;
    private javax.swing.JLabel b1;
    private javax.swing.JLabel b2;
    private javax.swing.JButton btnStartNew;
    private javax.swing.JButton btnStartServer;
    private javax.swing.JLabel c0;
    private javax.swing.JLabel c1;
    private javax.swing.JLabel c2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    public static javax.swing.JTextField txtClport;
    public static javax.swing.JTextField txtIpAdd;
    public static javax.swing.JTextField txtServerPort;
    // End of variables declaration//GEN-END:variables

    public void sendPoint(String point) {
        try {
            point = "#" + point + "#";
            port = Integer.parseInt(txtClport.getText());
            MainClient.mySendCommand(point, txtIpAdd.getText(), port);
        } catch (Exception ex) {
            System.out.println("Error");
        }
    }

    public void setPoint(String pointAll) {
        String point = pointAll.split("!")[0];
        String type = pointAll.split("!")[1];
        setClick(point, type);
    }

    public void setClick(String point, String type) {
        System.out.println("Seting clicks point is - " + point + "  Type is - " + type);
        if (point.equals("A0")) {
            a0.setText(type);
            A.put(0, type);
        } else if (point.equals("A1")) {
            a1.setText(type);
            A.put(1, type);
        } else if (point.equals("A2")) {
            a2.setText(type);
            A.put(2, type);
        } else if (point.equals("B0")) {
            b0.setText(type);
            B.put(0, type);
        } else if (point.equals("B1")) {
            b1.setText(type);
            B.put(1, type);
        } else if (point.equals("B2")) {
            b2.setText(type);
            B.put(2, type);
        } else if (point.equals("C0")) {
            c0.setText(type);
            C.put(0, type);
        } else if (point.equals("C1")) {
            c1.setText(type);
            C.put(1, type);
        } else if (point.equals("C2")) {
            c2.setText(type);
            C.put(2, type);
        }
        clickTimes++;
        Player1Times++;
        checkWinings();
    }

    private void startNewGame() {
        A.put(0, "");
        A.put(1, "");
        A.put(2, "");
        B.put(0, "");
        B.put(1, "");
        B.put(2, "");
        C.put(0, "");
        C.put(1, "");
        C.put(2, "");
        Player1Times = 0;
        Player2Times = 0;
        clickTimes = 0;
        a0.setText("###");
        a1.setText("###");
        a2.setText("###");
        b0.setText("###");
        b1.setText("###");
        b2.setText("###");
        c0.setText("###");
        c1.setText("###");
        c2.setText("###");
    }
    ServerSocket ssocket;
    Runnable sever = new Runnable() {

        public void run() {
            while (true) {
                try {
                    Thread.sleep(100);
                    Socket inSocket = ssocket.accept();
                    String ip = inSocket.getInetAddress().toString();
                    InputStream in = inSocket.getInputStream();
                    byte[] b = new byte[300];
                    in.read(b, 0, 255);
                    String msg = new String(b);
                    msg = msg.split("#")[1];
                    if (msg.equals("NewGame")) {
                        startNewGame();
                    } else {
                        setPoint(msg);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("#? Error ServerMainServer-ServerThread " + e);
                }
            }
        }
    };

    public void myStartServer() {//start server
        try {
            portServer = Integer.parseInt(txtServerPort.getText());
            ssocket = new ServerSocket(portServer);
            new Thread(sever).start();
            System.out.println("## SERVER HAS BEEN Started ...");
        } catch (Exception e) {
            System.out.println("#? Error ServerMainServer-myStartServer() " + e);
        }
    }
}


