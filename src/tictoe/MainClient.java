/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tictoe;

import java.io.OutputStream;
import java.net.Socket;

/**
 *
 * @author Amith Ishanka
 */
public class MainClient {

    public static void mySendCommand(String msg, String serverIp, int serverPort) throws Exception {//sending commands
        Socket outSocket = new Socket(serverIp, serverPort);
        byte[] b = msg.getBytes();
        //System.out.println("#@@@@@  Info Sending Command IP=" + serverIp + " , Port=" + serverPort + " , Msg=" + msg);
        OutputStream out = outSocket.getOutputStream();
        out.write(b);
        outSocket.close();
    }
}
