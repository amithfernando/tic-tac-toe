/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tictoe;

import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Amith Ishanka
 */
public class MainServer {

    ServerSocket ssocket;
    Runnable sever = new Runnable() {

        public void run() {
            while (true) {
                try {
                    Thread.sleep(100);
                    Socket inSocket = ssocket.accept();
                    String ip = inSocket.getInetAddress().toString();
                    InputStream in = inSocket.getInputStream();
                    byte[] b = new byte[300];
                    in.read(b, 0, 255);
                    String msg = new String(b);
                    Frame.txtIpAdd.setText(msg.split("#")[2]);
                    Frame.txtClport.setText(msg.split("#")[3]);
                    Frame.txtServerPort.setText(msg.split("#")[4]);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("#? Error ServerMainServer-ServerThread " + e);
                }
            }
        }
    };

    public void myStartServer() {//start server
        try {
            ssocket = new ServerSocket(1100);
            new Thread(sever).start();
            System.out.println("## SERVER HAS BEEN Started ...");
        } catch (Exception e) {
            System.out.println("#? Error ServerMainServer-myStartServer() " + e);
        }
    }
}
